const express = require('express');
const router = express.Router();

const notificationController = require('../controller/NotificationController');

router.get('/:driverPhone/:page/:limit', notificationController.getNotificationsByDriver);
router.post('/save', notificationController.saveNotification);
router.post('/read/:id', notificationController.readNotification);

module.exports = router;