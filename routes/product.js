const express = require('express');
const router = express.Router();

const productController = require('../controller/ProductController');

router.post('/store', productController.storeProduct);
router.get('/getAll', productController.getAllProducts);
router.get('/getProductById/:productId', productController.getProductById);
router.put('/updateProduct/:productId', productController.updateProductById);
module.exports = router;