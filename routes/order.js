const express = require('express');
const router = express.Router();

const orderController = require('../controller/OrderController');

router.post('/store', orderController.storeOrder);
router.get('/getAll', orderController.getOrders);
router.get('/getOrderCompleted/:driverId', orderController.getOrdersCompletedByDriver);
router.get('/getById/:orderId', orderController.getOrderByID);
router.get('/getByDriver/:driverPhone', orderController.getOrdersByDriver);
router.get('/getByDriverId/:driverId', orderController.getOrderByDriverId);
router.put('/changeStatus/:orderId', orderController.changeStatusOrder);
router.put('/updateOrder/:orderId', orderController.updateOrderById);
module.exports = router;