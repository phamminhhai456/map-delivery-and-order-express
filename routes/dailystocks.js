const express = require('express');
const router = express.Router();

const dailystockController = require('../controller/DailyStockController');

router.post('/store', dailystockController.storeDailyStocks);

module.exports = router;