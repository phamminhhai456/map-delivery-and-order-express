const express = require('express');
const router = express.Router();

const driverController = require('../controller/DriverController');

router.get('/getDriverById/:driverId', driverController.getDriverById);
router.get('/getDriverByPhone/:driverPhone', driverController.getDriverByPhone);
router.get('/getAllDriver', driverController.getAllDriver);
router.put('/updateLocId/:driverId', driverController.updateDriverLocId);
router.put('/updateDriverAddr/:driverId', driverController.updateDriverAddressToLocId);

module.exports = router;