const express = require('express');
const router = express.Router();

const authController = require('../controller/AuthController');

router.post('/login', authController.login);
router.post('/signup', authController.signup);
router.put('/forgot-password/:email',authController.forgotPassword);

module.exports = router;