const express = require('express');
const router = express.Router();

const userController = require('../controller/UserController');

router.get('/getListUser', userController.getUsers);
router.get('/:userId', userController.getUserById);
router.put('/update/:userId', userController.updateUser);
router.delete('/:userId', userController.deleteUser);


module.exports = router;