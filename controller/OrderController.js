const Order = require('../models/order');
const Driver = require('../models/driver');
const User = require('../models/user');
const mapbox = require('@mapbox/mapbox-sdk/services/geocoding');
const mapboxClient = mapbox({ accessToken: 'pk.eyJ1IjoiYnJvbHk5OTk4IiwiYSI6ImNscnI5cG14cTAxZ28ydnBjcGx3a2xkaWEifQ.GJLQq76LqceD19Q6abBjrQ'});

// Save order
const storeOrder = async (req,res) => {
    try {
        const {
            date,
            cusPhone,
            total,
            cash,
            status,
            orderNote,
            shipAddr,
            cusName,
            telno,
            driverPhone,
            web,
            products,
        } = req.body;

        const response = await mapboxClient.forwardGeocode({
            query: shipAddr,
            limit: 1
        }).send();

        // Convert ship address to locId
        if (response && response.body && response.body.features && response.body.features.length > 0) {
            const latitude = response.body.features[0].center[1];
            const longitude = response.body.features[0].center[0];

            // Assign coordinates to locID
            const newOrder = new Order({
                date,
                cusPhone,
                total,
                cash,
                status,
                orderNote,
                shipAddr,
                cusName,
                locID: [longitude, latitude], // Assign coordinates to locID
                telno,
                driverPhone,
                web,
                products,
            });

            await newOrder.save();

            return res.status(201).json({ success: true, message: 'Order created successfully' });
        } else {
            throw new Error('Geocoding failed');
        }

    } catch (error) {
        console.error('Error creating order:', error);

        return res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
};

// Get Order by Id
const getOrderByID = async (req,res) => {
    try {
        const id = req.params.orderId;

        const order = await Order.findById(id);

        if (!order) {
            return res.status(404).json({ message: 'Order does not exist' });
        } else {
            res.json({
                success: true,
                Order: order
            });
        }   
    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ success: false, message: 'An error occurred' });
    }
}

// Get List Order
const getOrders = async (req,res) => {
    try {
        const orders = await Order.find();
        res.json({
            success: true,
            orders: orders
        });
    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ success: false, message: 'An error occurred' });
    }
}

// Get list order of driver by driver phone
const getOrdersByDriver = async (req,res) => {
    try {
        const driverPhone = req.params.driverPhone;

        const orders = await Order.find({ driverPhone });

        return res.status(200).json({ success: true, orders });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
}

// Get list order of driver by driver id
const getOrderByDriverId = async (req,res) => {
   
    try {

        const id = req.params.driverId;

        console.log(id);
        
        const driver = await User.findById(id);

        if (!driver || driver.role !== 'driver') {
            return res.status(404).json({ message: 'Driver does not exist' });
        } else {
            const driverPhone = driver.phoneNo;

            const orders = await Order.find({ driverPhone });

            return res.json({
                success: true,
                Driver: driver,
                orders
            });
        }

    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: 'An error occured' });
    }
};

const getOrdersCompletedByDriver = async (req,res) => {
    try {
        const id = req.params.driverId;

        const driver = await User.findById(id);

        if(!driver || driver.role !== 'driver') {
            return res.status(404).json({ message: 'Driver does not exist'});
        } else {
            const driverPhone = driver.phoneNo;

            const orders = await Order.find({ driverPhone, status: 'Completed'});

            return res.json({
                success: true,
                Driver: driver,
                orders
            })
        }
    } catch (error) {
        return res.status(500).json({ message: 'An error occurred' });
    }
}

//Update order by order id
const updateOrderById = async (req,res) => {
    try {
        const orderId = req.params.orderId;

        const updateFields = req.body;

        const shipAddr = updateFields.shipAddr; 

        console.log(shipAddr);

        //Convert address to locId
        const response = await mapboxClient.forwardGeocode({
            query: shipAddr,
            limit: 1
        }).send();

        if(response && response.body && response.body.features && response.body.features.length > 0){
            const latitude = response.body.features[0].center[1];
            const longitude = response.body.features[0].center[0];

            updateFields.locID = [longitude, latitude];
        } else {
            throw new Error('Geocoding failed');
        }
        

        const updateOrder = await Order.findByIdAndUpdate(orderId, updateFields, {new : true});

        if(!updateOrder) {
            return res.status(404).json({ message: 'Order not found'});
        }

        return res.status(200).json({success: true, updateOrder});
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

// Update status order
const changeStatusOrder = async (req,res) => {
    const { orderId } = req.params;
    const { status } = req.body;

    try {

        const order = await Order.findByIdAndUpdate(orderId, { status }, { new: true});

        if(!order) {
            return res.status(404).json({ error : 'Order not found' });
        }

        return res.status(200).json(order);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: 'Internal server error' });
    }
}


module.exports = { storeOrder, getOrders, getOrderByID, getOrdersByDriver, changeStatusOrder, getOrderByDriverId, updateOrderById, getOrdersCompletedByDriver};
