const User = require('../models/user');

//Get user by user id
const getUserById = async (req,res) => {
   
    try {

        const id = req.params.userId;
        
        const user = await User.findById(id);

        if (!user) {
            return res.status(404).json({ message: 'User does not exist' });
        } else {
            res.json({
                success: true,
                User: user
            });
        }

    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'An error occured' });
    }
};

//Get list user
const getUsers = async (req,res) => {
    try {
        const users = await User.find(); 
        res.json({
            success: true,
            users
        }); 
    } catch (err) {
        console.error(err); 
        res.status(500).json({ message: 'Server error' }); 
    }
}

const updateUser = async (req,res) => {
    const userId = req.params.userId;
    const updates = req.body;

    console.log(userId)

    try {
        const user = await User.findByIdAndUpdate(userId, updates, { new: true });
        console.log(user);
        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }

        res.json(user);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
}

const deleteUser = async (req,res) => {
    const userId = req.params.userId;

    try {
        const user = await User.deleteOne({ _id: userId });
        // Kiểm tra kết quả xóa và trả về thông báo tương ứng
    if (result.deletedCount === 1) {
        res.json('User deleted successfully');
      } else {
        res.json('User not found');
      }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
}

module.exports = { getUserById, getUsers, updateUser, deleteUser };