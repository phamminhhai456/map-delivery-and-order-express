const mongoose = require('mongoose');
const Driver = require('../models/driver');
const User = require('../models/user');
const mapbox = require('@mapbox/mapbox-sdk/services/geocoding');
const mapboxClient = mapbox ({ accessToken: 'pk.eyJ1IjoiYnJvbHk5OTk4IiwiYSI6ImNscnI5cG14cTAxZ28ydnBjcGx3a2xkaWEifQ.GJLQq76LqceD19Q6abBjrQ'});

// Get Driver
const getDriverById = async (req,res) => {
   
    try {

        const id = req.params.driverId;
        
        const driver = await User.findById(id);

        if (!driver || driver.role !== 'driver') {
            return res.status(404).json({ message: 'Driver does not exist' });
        } else {
            res.json({
                success: true,
                Driver: driver
            });
        }

    } catch (error) {
        res.status(500).json({ message: 'An error occured' });
    }
};

const getDriverByPhone = async (req,res) => {
   
    try {

        const phone = req.params.driverPhone;
        
        const driver = await User.findOne({phoneNo: phone});

        if (!driver || driver.role !== 'driver') {
            return res.status(404).json({ message: 'Driver does not exist' });
        } else {
            res.json({
                success: true,
                Driver: driver
            });
        }

    } catch (error) {
        res.status(500).json({ message: 'An error occured' });
    }
};

//Get List Driver
const getAllDriver = async (req,res) => {
    try {
        const drivers = await User.find({ role: 'driver' });
        res.json({
            success: true,
            drivers: drivers
        });
    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ success: false, message: 'An error occurred' });
    }
}

//Update Driver 
const updateDriverLocId = async (req, res) => {
    try {
        const driverId = req.params.driverId;
        const locID = req.body.locID;

        const updatedDriver = await User.findByIdAndUpdate(driverId, { locID: locID }, { new: true });

        if (!updatedDriver) {
            return res.status(404).json({ success: false, message: 'Driver not found' });
        }

        res.status(200).json({ success: true, driver: updatedDriver });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
}

//Update driver address to locId
const updateDriverAddressToLocId = async (req, res) => {
    try {
        const driverId = req.params.driverId;
        const { currentAddress } = req.body;

        console.log(currentAddress);

        //Conver address to locID
        const response = await mapboxClient.forwardGeocode({
            query: currentAddress,
            limit: 1
        }).send();
        
        if(response && response.body && response.body.features && response.body.features.length > 0){
            const latitude = response.body.features[0].center[1];
            const longitude = response.body.features[0].center[0];

            const updateDriver = await User.findByIdAndUpdate(driverId, {
                $set: {
                    locID: [longitude, latitude]
                }
            }, {new : true});

            res.status(200).json({ success: true, message: 'Update successfully'});
        } else {
            res.status(404).json({ message: 'Geocoding failed' });
        }

    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
}

module.exports = { getDriverById,getAllDriver, updateDriverLocId, updateDriverAddressToLocId , getDriverByPhone};