const mongoose = require('mongoose');
const User = require('../models/user');

// Sign up
const signup = async (req, res) => {
    
    //Random pin code
    const generateRandomPin = () => {
        const randomPin = Math.floor(100000 + Math.random() * 900000);
        return randomPin;
    }
    
    const pin = generateRandomPin();
    
    try {
        
        const { username, name, password, phoneNo, email, role } = req.body;

        //Check user existed
        const existingUser = await User.findOne({ $or: [{ username }, { email }] });
        if (existingUser) {
            return res.status(400).json({ error: 'Username or email already exists' });
        }

        
        const newUser = new User({
            username,
            name,
            password,
            phoneNo,
            email,
            pin : pin,
            role
        });

       
        await newUser.save();

        
        res.status(201).json(newUser);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Server error' });
    }
}
    
//Login
const login = async (req,res) => {
    const { username, password } = req.body;

    try {
        
        const user = await User.findOne({ username, password });

        if (user) {
            res.json({
                success: true, 
                message: 'Login successful',
                User: user
            });
        } else {
            res.status(401).json({ message: 'Invalid username or password'});
        }
    } catch (error) {
        console.error('Error:', error); 
        res.status(500).json({ message: 'An error occurred' });
    }
}

//Forgot password
const forgotPassword = async (req,res) => {
    const { newPassword } = req.body;
    const email = req.params.email;

    try {
        const user = await User.findOne({ email: email });

        if (!user) {
            return res.status(404).json({ message: 'User does not exist' });
        }

        user.password = newPassword;

        await user.save();
        res.status(200).json({ message: 'Password updated successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'An error occured' });
    }
}

//logout
const logout = async (req,res) => {
    try {
        res.json({ success: true, message: 'Logout successful' });
    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: 'An error occurred' });
    }
} 

module.exports = {login,signup,forgotPassword};