const Product = require('../models/product');

//Save Product
const storeProduct = async (req, res) => {

    try {

        const { sku, name, qTy, weight, price, prodNote, rate } = req.body;

        const existingProduct = await Product.findOne({ name });

        if (existingProduct) {
            return res.status(400).json({ message: 'Product already exists' });
        }

        const newProduct = new Product({ sku, name, qTy, weight, price, prodNote, rate });

        await newProduct.save();

        res.json({ message: 'Store successful' });
        
    } catch (error) {
        console.log(error);
    }
} 

//Get list products
const getAllProducts = async (req,res) => {
    try {
        const products = await Product.find();
        res.json({
            success: true,
            products: products
        });

    } catch (error) {
        console.log(error);
    }
}

//Get product by id
const getProductById = async (req,res) => {
    try {
        const id = req.params.productId;

        const product = await Product.findById(id);

        if(!product) {
            return res.status(404).json({ message: 'Product does not exist' });
        } else {
            res.json({
                success: true,
                Products: product
            });
        }
    } catch (error) {
        console.log(error);
    }
}

//Update product by product id
const updateProductById = async (req,res) => {
    try {
        const orderId = req.params.productId;

        const updateFields = req.body;

        const updateProduct = await Product.findByIdAndUpdate(orderId, updateFields, {new : true});

        if(!updateProduct) {
            return res.status(404).json({ message: 'Product not found'});
        }

        return res.status(200).json({success: true, updateProduct});
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: 'Internal server error' });
    }
}

module.exports = { storeProduct, getAllProducts, getProductById, updateProductById };