const Notification = require("../models/notification");

const saveNotification = async (req, res) => {
  try {
    const {
      id,
      sender,
      receiver,
      orderId,
      shipAddress,
      driverPhone,
      content,
      type,
    } = req.body;

    const newRecord = new Notification({
      id,
      sender,
      receiver,
      orderId,
      shipAddress,
      driverPhone,
      content,
      type,
    });

    const savedRecord = await newRecord.save();

    return res.status(200).json({ success: true, data: savedRecord });
  } catch (error) {
    console.error("Error saving notification:", error);

    return res
      .status(500)
      .json({ success: false, message: "Internal Server Error" });
  }
};

const getNotificationsByDriver = async (req, res) => {
  try {
    const page = parseInt(req.params.page);
    const limit = parseInt(req.params.limit);

    const startIndex = page * limit;
    const endIndex = page * limit;

    const data = await Notification.find({
      driverPhone: req.params.driverPhone,
    })
      .sort({ read: 1 })
      .sort({ createdAt: "desc" })
      .skip(startIndex)
      .limit(limit);

    const total = await Notification.countDocuments({
      driverPhone: req.params.driverPhone,
    });

    res.json({
      success: true,
      page,
      limit,
      total,
      data,
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: "Server error" });
  }
};

const readNotification = async (req, res) => {
  try {
    const id = req.params.id;

    // Tìm kiếm bản ghi thông báo dựa trên driverPhone
    const notification = await Notification.findOne({ id });

    if (!notification) {
      return res
        .status(404)
        .json({ success: false, message: "Notification not found" });
    }

    notification.read = true;
    await notification.save();

    return res.status(200).json({ success: true, data: notification });
  } catch (error) {
    console.error("Error updating notification type:", error);

    return res
      .status(500)
      .json({ success: false, message: "Internal Server Error" });
  }
};

module.exports = {
  saveNotification,
  getNotificationsByDriver,
  readNotification,
};
