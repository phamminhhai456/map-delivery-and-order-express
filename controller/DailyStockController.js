const DailyStocks = require('../models/dailystock');

//Save dailystocks
const storeDailyStocks = async (req,res) => {
    try {
        const { phoneNo, created, items } = req.body;

        
        const newDailyStocks = new DailyStocks({
            phoneNo,
            created,
            items,
        });

        await newDailyStocks.save();

        return res.status(201).json({ success: true, message: 'DailyStocks created successfully' });
    } catch (error) {
        console.error('Error creating dailyStocks:', error);

        return res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
}

module.exports = { storeDailyStocks };