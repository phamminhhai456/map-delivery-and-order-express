const express = require("express");
const app = express();

const port = 3000;
const authRouter = require("./routes/auth");
const driverRouter = require("./routes/driver");
const orderRouter = require("./routes/order");
const productRouter = require("./routes/product");
const dailyStockRouter = require("./routes/dailystocks");
const userRouter = require("./routes/user");
const notificationRouter = require("./routes/notification");
const cors = require("cors");
const WebSocket = require("ws");
const http = require("http");
const server = http.createServer(app);
// const wss = new WebSocket.Server({ server })
const { Server } = require("socket.io");
const { socketHandler } = require("./socket");

var db = require("./database/database");
//Connect to db
db.connectDb();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({
  origin: '*'
}));

//Route init
app.use("/api/auth/", authRouter);
app.use("/api/driver/", driverRouter);
app.use("/api/order/", orderRouter);
app.use("/api/product/", productRouter);
app.use("/api/dailystock/", dailyStockRouter);
app.use("/api/user/", userRouter);
app.use("/api/notifications/", notificationRouter);

app.get("/", (req, res) => {
  res.json({ message: "Welcome to CRUD application." });
});

server.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

// [052403Tree] set up socket io
const io = new Server(server, {
  pingTimeout: 60000,
  cors: {
    origin: "*",
  },
});
socketHandler(io);
