var mongoose = require('mongoose');
const config = require('config');

// connect db
exports.connectDb = async () => {
    const dbConfig = config.get('mongodb');
    await mongoose.connect(dbConfig.host)
    .then(() => {
        console.log('Database connected');
    })
    .catch((error) => {
        console.log('Error connecting to database');
    });
}