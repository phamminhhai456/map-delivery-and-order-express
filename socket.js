let onlineUsers = [];

const addUser = (userId, socketId) => {
  !onlineUsers.some((user) => user.userId === userId) &&
    onlineUsers.push({ userId, socketId });
};

const removeUser = (socketId) => {
  onlineUsers = onlineUsers.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
  return onlineUsers.find((user) => user.userId === userId);
};

const socketHandler = (io) => {
  io.on("connection", (socket) => {
    console.log(`User ${socket.id} connected to socket.io`);
    // set up
    socket.on("addUser", ({ userId }) => {
      addUser(userId, socket.id);
      console.log('added user: ' + userId);
      socket.join(userId); // add every user to new room
    });

    socket.on("locationUpdate", (data) => {
      // Xử lý dữ liệu vị trí được gửi từ máy khách
      const latitude = data.latitude;
      const longitude = data.longitude;
      console.log(latitude, longitude);
      let receiverUser = getUser(data.receiver);
      socket.to(receiverUser?.socketId).emit("locationUpdate", { latitude, longitude, receiver: data.receiver });
    });

    socket.on("routeUpdate", (data) => {
      let receiverUser = getUser(data.receiver);
      console.log(data);
      socket.to(receiverUser?.socketId).emit("routeUpdate", data);
      if(data?.distance < 500.00){
        socket.emit("driver-arrived", {arrived: true});
      }
    });

    // DELIVERY NOTIFICATIONS
    socket.on("send-notifications", (data) => {
      let receiverUser = getUser(data.receiver);
      console.log(data);
      socket.to(receiverUser?.socketId).emit("get-notifications", data);
    })

    socket.on("order-cancel", (data) => {
      let receiverUser = getUser(data.receiver);
      console.log(data);
      socket.to(receiverUser?.socketId).emit("order-cancel", data);
    })

    socket.on("order-inprogress", (data) => {
      let receiverUser = getUser(data.receiver);
      console.log(data);
      socket.to(receiverUser?.socketId).emit("get-order-inprogress", data);
    })

    socket.on("test", (data) => {
      console.log(data);
    })

    // disconnected
    socket.on("disconnect", () => {
      console.log(`User ${socket.id} disconnected...`);
      removeUser(socket.id);
    });
  });
};

module.exports = { socketHandler };
