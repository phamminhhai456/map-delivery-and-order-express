const mongoose = require("mongoose");

const notificationSchema = new mongoose.Schema(
  {
    id: {
      type: String,
    },
    sender: {
      //   type: mongoose.Types.ObjectId,
      //   ref: "User",
    },
    receiver: {
      //   type: mongoose.Types.ObjectId,
      //   ref: "User",
    },
    orderId: {
      type: String,
    },
    driverPhone: {
      type: String,
    },
    content: {
      type: String,
    },
    shipAddress: {
      type: String,
    },
    type: {
      type: String,
      //   required: true,
    },
    read: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const Notification =
  mongoose.models.Notification ||
  mongoose.model("Notification", notificationSchema);

module.exports = Notification;
