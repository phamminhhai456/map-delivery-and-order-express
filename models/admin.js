const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const Admins = new Schema({
    username: String,
    name: String,
    password: String,
    phoneNo: String,
    status: {
        type: String,
        default: 'active'
    },
    email: String,
    pin: Number,
    role: String,
},{
    timestamps : true
});

module.exports = mongoose.model('Admin', Admins);