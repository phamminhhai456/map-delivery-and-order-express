const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Products = new Schema({
    sku: String,
    name: String,
    qTy: String,
    weight: String,
    price: String,
    prodNote: String,
    rate: String,
},{
    timestamps : true
});

module.exports = mongoose.model("Product", Products);