const mongoose = require('mongoose');

const Item = mongoose.Schema({
  sku: String,
  cat: String,
  name: String,
  qTy1: String,
  qTy2: String,
  qTy3: String,
  qTy14: String,
  qTy28: String,
  eqTy: String,
});

const DailyStocks = mongoose.Schema({
  phoneNo: String,
  created: Date,
  items: [Item]
});
module.exports = mongoose.model('dailyStocks', DailyStocks);