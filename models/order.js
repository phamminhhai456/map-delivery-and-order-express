const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const Products = new Schema({
    sku: String,
    name: String,
    qTy: String,
    weight: String,
    price: String,
    prodNote: String,
    rate: String,
},{
    timestamps : true
});

const Orders = new Schema({
    date: String,
    cusPhone: String,
    total: String,
    cash: String,
    status: String,
    orderNote: String,
    shipAddr: String,
    cusName: String,
    telno: String,
    driverPhone: String,
    locID: [],
    web: String,
    products: [Products]
},{
    timestamps : true
});

module.exports = mongoose.model('Order', Orders);