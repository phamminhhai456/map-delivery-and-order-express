const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const Drivers = new Schema({
    username: String,
    name: String,
    password: String,
    phoneNo: String,
    status: {
        type: String,
        default: 'active'
    },
    email: String,
    pin: Number,
    role: String,
    paid: {
        type: Number,
        default: 0
    },
    total: {
        type: Number,
        default: 0
    },
    locID: {
        type: [],
        default: [0,0]
    }
},{
    timestamps : true
});

module.exports = mongoose.model('Driver', Drivers);