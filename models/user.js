const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const User = new Schema({
    username: String,
    name: String,
    password: String,
    phoneNo: String,
    status: {
        type: String,
        default: 'active'
    },
    email: String,
    pin: Number,
    role: String,
    // Driver Fields
    paid: {
        type: Number,
        default: 0,
    },
    total: {
        type: Number,
        default: 0,
    },
    locID: {
        type: [],
        default: [0,0],
    },
    shopID: {
        type: String,
    }
},{
    timestamps : true
});

module.exports = mongoose.model('User', User);
